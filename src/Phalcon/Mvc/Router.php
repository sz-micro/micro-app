<?php

namespace GranitSDK\Phalcon\Mvc;


class Router extends \Phalcon\Mvc\Router
{
	public function __construct($defaultRoutes = true)
	{
		parent::__construct($defaultRoutes);

		$this->add('/check', 'GranitSDK\\Controller\\Check::index');
	}
}