<?php

namespace GranitSDK\Controller;

use Phalcon\Mvc\Controller;

class CheckController extends Controller
{
	public function indexAction()
	{
		$this->response->setStatusCode('200');
		$this->response->setContent('ok');

		return $this->response;
	}
}